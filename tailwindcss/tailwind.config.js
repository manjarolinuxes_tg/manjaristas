module.exports = {
  content: ['../**/*.html'],
  theme: {
    extend: {
      colors: {
        'brand_dark': '#367755',
        'brand_medium': '#68BF89',
        'brand_light': '#AAD8B5',
        'brand_dark_complementary': '#bd6e97',
      }
    },
  },
  plugins: [
    require('@tailwindcss/typography'),
  ],
}
