from .base import *

DEBUG = False
SITE_PREFIX = ""

try:
    from .local import *
except ImportError:
    pass


if SITE_PREFIX:
    WAGTAILADMIN_BASE_URL += f"/{SITE_PREFIX.strip('/')}"