from django.dispatch import receiver
from wagtail.admin.signals import init_new_page
from .models import Article
from wagtail.admin.views.pages.create import CreateView

@receiver(init_new_page, sender=CreateView)
def set_default_author(sender, page, parent, **kwargs):
    if isinstance(page, Article):
        # This signal is emitted on page creation, so author is going to be always Null/None, as defined in the model
        page.author = page.owner

