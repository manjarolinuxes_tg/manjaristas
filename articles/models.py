from django import forms
from django.db import models
from wagtail.models import Page
from wagtail.fields import RichTextField
from wagtail.admin.panels import FieldPanel, InlinePanel, MultiFieldPanel
from wagtail.search import index
from modelcluster.fields import ParentalKey, ParentalManyToManyField
from modelcluster.contrib.taggit import ClusterTaggableManager
from wagtail.snippets.models import register_snippet
from taggit.models import TaggedItemBase, TagBase
from django.utils.translation import gettext_lazy as _
from django.contrib.auth import get_user_model
from django.conf import settings
from django_extensions.db.fields import AutoSlugField
from home.models import HomePage

@register_snippet
class ArticleCategory(models.Model):
    name = models.CharField(max_length=255)
    slug = AutoSlugField(populate_from="name", editable=True, help_text="Optional, this will be autopopulated if you left it blank")
    icon = models.ForeignKey(
        'wagtailimages.Image', null=True, blank=True,
        on_delete=models.SET_NULL, related_name='+'
    )

    @property
    def url(self):
        home = HomePage.objects.first()
        return home.url + home.reverse_subpage('categories', args=(self.slug,))

    panels = [
        FieldPanel('name'),
        FieldPanel('slug'),
        FieldPanel('icon'),
    ]

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Categories'

class ArticlesTag(TagBase):
    @property
    def url(self):
        home = HomePage.objects.first()
        return home.url + home.reverse_subpage('tags', args=(self.slug,))

    class Meta:
        verbose_name = "articles tag"
        verbose_name_plural = "articles tags"

class TaggedArticle(TaggedItemBase):
    tag = models.ForeignKey(ArticlesTag, related_name="tagged_articles", on_delete=models.CASCADE)
    content_object = ParentalKey('Article', on_delete=models.CASCADE)

def get_site_owner():
    return get_user_model().objects.filter(is_superuser=True).first().id

class Article(Page):
    date = models.DateField(verbose_name=_('Post date'))
    intro = models.CharField(max_length=250)
    body = RichTextField(blank=True)
    tags = ClusterTaggableManager(through=TaggedArticle, blank=True)
    categories = ParentalManyToManyField('ArticleCategory', blank=True, verbose_name=_('Categoría'))
    author = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_('Author'), on_delete=models.SET_DEFAULT, default=get_site_owner, blank=False, null=False)
    feed_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    search_fields = Page.search_fields + [
        index.SearchField('intro'),
        index.SearchField('body'),
    ]

    content_panels = Page.content_panels + [
        MultiFieldPanel([
            FieldPanel('date'),
            FieldPanel('tags'),
            FieldPanel('categories', widget=forms.CheckboxSelectMultiple),
        ], heading="Metadata"),
        FieldPanel('intro'),
        FieldPanel('body'),
    ]

    promote_panels = [
        MultiFieldPanel(Page.promote_panels, "Common page configuration"),
        FieldPanel('feed_image'),
        FieldPanel('author'),
    ]

    def get_context(self, request, *args, **kwargs):
        context = super().get_context(request, *args, **kwargs)

        # Add extra variables and return the updated context
        all_articles = list(Article.objects.all().live())[::-1]
        this_article = all_articles.index(self)
        context['next_article'] = all_articles[this_article + 1] if this_article < len(all_articles)-1 else None
        context['previous_article'] = all_articles[this_article - 1] if this_article > 0 else None
        context['author'] = self.author
        return context