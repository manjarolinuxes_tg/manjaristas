from django.db import models

from wagtail.admin.panels import FieldPanel, MultiFieldPanel
from wagtail.snippets.models import register_snippet
from wagtail.fields import RichTextField
from wagtail.models import Orderable
from modelcluster.fields import ParentalKey
from modelcluster.models import ClusterableModel
from wagtail.admin.panels import MultiFieldPanel, InlinePanel

@register_snippet
class TextWidget(models.Model):
    title = models.CharField(max_length=255, verbose_name='Título')
    text = RichTextField(blank=True, verbose_name="texto")
    CTA = models.CharField(max_length=100, blank=True, verbose_name="Texto del botón")
    CTA_URL = models.URLField(verbose_name='Enlace del botón', blank=True)

    panels = [
        FieldPanel('title'),
        FieldPanel('text'),
        MultiFieldPanel([
            FieldPanel('CTA'),
            FieldPanel('CTA_URL'),
        ], heading="Botón CallToAction"),
    ]

    def __str__(self):
        return self.title

class Link(Orderable):
    text = models.CharField(verbose_name='Link text', blank=True, null=True, max_length=50)
    url = models.URLField(verbose_name='Link URL')

    page = ParentalKey('LinksWidget', related_name='links')

    panels = [
        FieldPanel('text'),
        FieldPanel('url'),
    ]

@register_snippet
class LinksWidget(ClusterableModel):
    """The linksWidget clusterable model."""

    title = models.CharField(max_length=100)
    text = RichTextField(blank=True, verbose_name='Widget text')
    # slug = AutoSlugField(populate_from='title', editable=True)

    panels = [
        MultiFieldPanel([
            FieldPanel('title'),
            FieldPanel('text'),
        ], heading='Link group'),
        InlinePanel('links', label='Link')
    ]

    def __str__(self):
        return self.title