from django.db import models

from wagtail.admin.panels import FieldPanel
from wagtail.snippets.models import register_snippet
from wagtail.fields import RichTextField
from wagtail.contrib.settings.models import BaseSetting, register_setting
from menus.models import Menu
from django.core.validators import MinValueValidator, MaxValueValidator


@register_snippet
class Footer(models.Model):
    title = models.CharField(max_length=255, verbose_name='Título')
    text = RichTextField(blank=True, verbose_name="texto")

    panels = [
        FieldPanel('title'),
        FieldPanel('text'),
    ]

    def __str__(self):
        return self.title


@register_setting
class FooterSettings(BaseSetting):
    footer_text = models.ForeignKey(verbose_name='Texto del footer', to=Footer, on_delete=models.PROTECT, blank=True, null=True, related_name='+')
    copyright_text = models.CharField(blank=True, verbose_name='Texto del Copyright', max_length=100)

    class Meta:
        verbose_name = "Configuraciones del footer"

@register_setting
class MenuSettings(BaseSetting):
    navigation_menu = models.ForeignKey(verbose_name='Menú de navegación principal', to=Menu, on_delete=models.PROTECT, blank=True, null=True, related_name='+')
    footer_menu = models.ForeignKey(verbose_name='Menú del footer', to=Menu, on_delete=models.PROTECT, blank=True, null=True, related_name='+')

    class Meta:
        verbose_name = "Configuraciones de menus"

@register_setting
class SiteSettings(BaseSetting):
    site_logo = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
    )
    articles_per_page = models.PositiveSmallIntegerField(verbose_name='Artículos por página', default=10, validators=[MinValueValidator(0), MaxValueValidator(20)])

    class Meta:
        verbose_name = "Configuraciones de sitio web"
