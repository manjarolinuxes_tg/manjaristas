from django import forms
from django.utils.translation import gettext_lazy as _
from wagtail.users.forms import UserEditForm, UserCreationForm
from wagtail.images.widgets import AdminImageChooser
from wagtail.images import get_image_model
from wagtail.fields import RichTextField
# from wagtail.admin.panels import FieldPanel


class CustomUserEditForm(UserEditForm):
    # bio = forms.CharField(required=False, label=_("Bio"))
    bio = RichTextField(blank=True, verbose_name='Bio')
    profile_pic = forms.ModelChoiceField(queryset=get_image_model().objects.all(), widget=AdminImageChooser(), required=False, label=_("Profile picture"))
    telegram_link = forms.URLField(required=False)
    twitter_link = forms.URLField(required=False)
    linkedin_link = forms.URLField(required=False)
    instagram_link = forms.URLField(required=False)

class CustomUserCreationForm(UserCreationForm):
    bio = forms.CharField(required=False, label=_("Bio"))
    profile_pic = forms.ModelChoiceField(queryset=get_image_model().objects.all(), widget=AdminImageChooser(), required=False, label=_("Profile picture"))
    telegram_link = forms.URLField(required=False)
    twitter_link = forms.URLField(required=False)
    linkedin_link = forms.URLField(required=False)
    instagram_link = forms.URLField(required=False)

