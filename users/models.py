from django.db import models
from django.contrib.auth.models import AbstractUser
from django_extensions.db.fields import AutoSlugField
# from home.models import HomePage
from wagtail.models import Site
from wagtail.fields import RichTextField
# from wagtail.admin.panels import FieldPanel


class User(AbstractUser):
    # bio = models.CharField(verbose_name='Bio', max_length=1000)
    bio = RichTextField(verbose_name='Bio', max_length=1000)
    slug = AutoSlugField(populate_from=['first_name', 'last_name'], blank=False, editable=True)
    profile_pic = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
    )
    telegram_link = models.URLField(verbose_name='Enlace Telegram', blank=True, null=True)
    twitter_link = models.URLField(verbose_name='Enlace Twitter', blank=True)
    linkedin_link = models.URLField(verbose_name='Enlace LinkedIn', blank=True)
    instagram_link = models.URLField(verbose_name='Enlace Instagram', blank=True)

    @property
    def url(self):
        home_url = Site.objects.first().root_url
        # home = HomePage.objects.first()
        # return home.url + home.reverse_subpage('authors', args=(self.slug,))
        return f"{home_url}/author/{self.slug}"


    def display_name(self):
        return f'{self.first_name} {self.last_name}'
