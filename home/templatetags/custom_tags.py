from django import template
from babel.dates import format_date

register = template.Library()

@register.simple_tag
def localize_date(date, locale):
    return format_date(date, locale=locale, format='long')