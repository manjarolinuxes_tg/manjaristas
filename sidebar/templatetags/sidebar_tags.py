from django import template
from sidebar.models import TextWidget, LinksWidget
from articles.models import ArticleCategory

register = template.Library()

# All TextWidgets snippet
@register.inclusion_tag('sidebar/tags/all_text_widgets.html', takes_context=True)
def all_text_widgets(context):
    return {
        'text_widgets': TextWidget.objects.all(),
        'request': context['request'],
    }

# Single TextWidget snippet
@register.inclusion_tag('sidebar/tags/text_widget.html', takes_context=True)
def text_widget(context, title):
    return {
        'widget': TextWidget.objects.get(title=title),
        'request': context['request'],
    }

# Single LinksWidget snippet
@register.inclusion_tag('sidebar/tags/links_widget.html', takes_context=True)
def links_widget(context, title):
    return {
        'widget': LinksWidget.objects.get(title=title),
        'request': context['request'],
    }

# Categories snippet
@register.inclusion_tag('sidebar/tags/categories.html', takes_context=True)
def categories(context):
    return {
        'categories': ArticleCategory.objects.all(),
        'request': context['request'],
    }

