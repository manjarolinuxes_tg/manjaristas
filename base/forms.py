from django import forms
from django.contrib.auth import get_user_model


class CustomSettingsForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        fields = ['bio', 'profile_pic', 'telegram_link', 'twitter_link', 'linkedin_link', 'instagram_link']