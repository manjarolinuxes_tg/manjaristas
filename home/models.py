from django.apps import apps
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from wagtail.models import Page
from wagtail.fields import RichTextField
from wagtail.admin.panels import FieldPanel
from wagtail.contrib.routable_page.models import RoutablePageMixin, path
from base.models import SiteSettings
from django.shortcuts import get_object_or_404
from users.models import User


class HomePage(RoutablePageMixin, Page):
    template = "home/homepage.html"
    page_description = "Página principal del site"
    preface = RichTextField(blank=True)

    def render_list_view(self, request, articles, author=None, title=None, category=None, tag=None):
        paginator = Paginator(articles, SiteSettings.for_request(request).articles_per_page)
        # Try to get the ?page=x value
        page = request.GET.get("page")
        try:
            # If the page exists and the ?page=x is an int
            articles = paginator.page(page)
            page = int(page)
        except PageNotAnInteger:
            # If the ?page=x is not an int; show the first page
            page = 1
            articles = paginator.page(page)

        except EmptyPage:
            # If the ?page=x is out of range (too high most likely)
            # Then return the last page
            page = paginator.num_pages
            articles = paginator.page(page)

        return self.render(request, context_overrides={
            'articles': articles,
            'current_page': page,
            'page_count': paginator.num_pages,
            'pages': range(1, paginator.num_pages + 1),
            'next_page': page + 1,
            'previous_page': page - 1,
            'author': author,
            'title': title,
            'is_author': True if author else False,
            'category': category,
            'tag': tag,
        })

    @path('')
    def default(self, request):
        article_model = apps.get_model('articles.Article')
        articles = self.get_children().live().type(article_model).order_by('-first_published_at')
        return self.render_list_view(request, articles)
   
    @path('category/<str:category>/')
    def categories(self, request, category='Noticias'):
        article_model = apps.get_model('articles.Article')
        articles = self.get_children().live().type(article_model).filter(article__categories__slug=category).order_by('-first_published_at')
        return self.render_list_view(request, articles, title=f'Categoría: {category}', category=category)

    @path('tags/<str:tag>/')
    def tags(self, request, tag):
        article_model = apps.get_model('articles.Article')
        articles = self.get_children().live().type(article_model).filter(article__tags__slug=tag).order_by('-first_published_at')
        return self.render_list_view(request, articles, title=f'Tag: {tag}', tag=tag)
    
    @path('author/<str:author>/')
    def authors(self, request, author):
        author_obj = get_object_or_404(User, slug=author)
        article_model = apps.get_model('articles.Article')
        articles = self.get_children().live().type(article_model).filter(article__author=author_obj).order_by('-first_published_at')
        return self.render_list_view(request, articles, author=author_obj, title='Autor')


    content_panels = Page.content_panels + [
        FieldPanel('preface'),
    ]

    class Meta:
        verbose_name = "homepage"
