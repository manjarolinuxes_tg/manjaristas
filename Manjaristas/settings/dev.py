from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "django-insecure-afosa8v3eh=h0%_0cn0ljequ=$@-vzi0&)v3@_ffqq4(&7nes*"

# SECURITY WARNING: define the correct hosts in production!
ALLOWED_HOSTS = ["*"]

EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"

SITE_PREFIX = ""

try:
    from .local import *
except ImportError:
    pass

if SITE_PREFIX:
    WAGTAILADMIN_BASE_URL += f"/{SITE_PREFIX.strip('/')}"